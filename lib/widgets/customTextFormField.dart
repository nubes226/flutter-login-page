import 'package:flutter/material.dart';
import 'package:login_app/colors/color.dart';

class CustomTextFormField extends StatelessWidget {
  final Color borderColor;
  final Color textColor;
  final String hintText;
  final IconData? prefixIcon;
  final IconData? suffixIcon;
  const CustomTextFormField({
    super.key,
    this.borderColor = primaryColor,
    this.textColor = primaryColor,
    this.hintText = '',
    this.prefixIcon,
    this.suffixIcon,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12),
        border: Border.all(
          color: borderColor,
          width: 2,
        ),
      ),
      child: TextFormField(
        style: TextStyle(
          fontSize: 20,
          color: textColor,
        ),
        decoration: InputDecoration(
          hintText: hintText,
          prefixIconColor: textColor,
          suffixIconColor: textColor,
          hintStyle: TextStyle(
            fontSize: 16,
            color: textColor,
          ),
          border: InputBorder.none,
          prefixIcon: prefixIcon != null
              ? Icon(
                  prefixIcon,
                  color: textColor,
                )
              : const SizedBox.shrink(),
          suffixIcon: suffixIcon != null
              ? Icon(
                  suffixIcon,
                  color: textColor,
                )
              : const SizedBox.shrink(),
        ),
      ),
    );
  }
}
