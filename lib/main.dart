import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:login_app/pages/signin.page.dart';
import 'package:login_app/themes/custom.theme.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
      const SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
      ),
    );
    return MaterialApp(
      title: 'Login Page',
      theme: CustomTheme.theme,
      debugShowCheckedModeBanner: false,
      home: const SignInPage(),
    );
  }
}
