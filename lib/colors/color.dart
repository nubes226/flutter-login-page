import 'package:flutter/material.dart';

const MaterialColor primarySwatch = MaterialColor(
  _primarySwatchValue,
  <int, Color>{
    50: Color(0xFF000000),
    100: Color(0xFF000000),
    200: Color(0xFF000000),
    300: Color(0xFF000000),
    400: Color(0xFF000000),
    500: Color(_primarySwatchValue),
    600: Color(0xFF000000),
    700: Color(0xFF000000),
    800: Color(0xFF000000),
    900: Color(0xFF000000),
  },
);
const int _primarySwatchValue = 0xFF1CA2DD;

const Color primaryColor = Color(0xFF1CA2DD);
const Color secondaryColor = Color(0xFFE30613);

const Color tertiaryColor = Color(0xFFFF7F27);
const Color grisColor = Color(0xFFE3E5E6);
const Color darkColor = Color(0xFF000000);
const Color whiteColor = Color(0xFFFFFFFF);
const Color greenColor = Colors.green;

const Color successColor = Colors.green;
const Color dangerColor = Color(0xFFE30613);
const Color warningColor = Color(0xFFFF7F27);
