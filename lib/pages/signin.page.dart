import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:login_app/colors/color.dart';
import 'package:login_app/widgets/customButton.widget.dart';
import 'package:login_app/widgets/customTextFormField.dart';

class SignInPage extends StatelessWidget {
  const SignInPage({super.key});

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: primaryColor,
      body: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(
              height: (height / 5) * 3,
              child: SafeArea(
                child: Container(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 16,
                    vertical: 24,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      const SizedBox(
                        height: 24,
                      ),
                      const Text(
                        "Bienvenue !",
                        style: TextStyle(
                          fontSize: 32,
                          fontWeight: FontWeight.w600,
                          color: grisColor,
                        ),
                      ),
                      const SizedBox(
                        height: 32,
                      ),
                      ClipRRect(
                        borderRadius: BorderRadius.circular(100),
                        child: Image.asset(
                          "assets/images/brand.jpg",
                          height: 176,
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
            ClipRRect(
              borderRadius: const BorderRadius.only(
                topLeft: Radius.circular(24),
                topRight: Radius.circular(24),
              ),
              child: Container(
                height: (height / 5) * 2 - 75,
                padding: const EdgeInsets.fromLTRB(12, 8, 12, 0),
                color: whiteColor,
                child: Column(
                  children: const [
                    SizedBox(height: 24),
                    Text(
                      'Renseigner vos informations',
                      textAlign: TextAlign.start,
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    SizedBox(height: 16),
                    CustomTextFormField(
                      prefixIcon: FontAwesomeIcons.userTie,
                      hintText: 'Login',
                    ),
                    SizedBox(height: 16),
                    CustomTextFormField(
                      prefixIcon: Icons.lock_outline,
                      hintText: 'Mot de passe',
                      suffixIcon: Icons.visibility,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: SizedBox(
        height: 75,
        child: Container(
          color: whiteColor,
          padding: const EdgeInsets.symmetric(
            horizontal: 16,
            vertical: 8,
          ),
          child: CustomButton(
            btnLabel: 'Se connecter',
            btnColor: primaryColor,
            onPressed: () {},
          ),
        ),
      ),
    );
  }
}
