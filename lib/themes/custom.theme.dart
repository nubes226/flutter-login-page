import 'package:flutter/material.dart';
import 'package:login_app/colors/color.dart';

class CustomTheme {
  static ThemeData theme = ThemeData(
    primarySwatch: primarySwatch,
    primaryColor: primaryColor,
    scaffoldBackgroundColor: grisColor,
  );

  static PreferredSize defaultAppBar = PreferredSize(
    preferredSize: const Size.fromHeight(2),
    child: Container(
      color: grisColor,
    ),
  );
}
